const letters = document.getElementById("letterDiv");
const words = document.getElementById("wordsDiv");
let letterCounts = {};
let wordCounts = {};


document.getElementById("countButton").onclick = function() {
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    //clear letter space
    let myNode1 = document.getElementById("lettersDiv");
    while (myNode1.firstChild) {
        myNode1.removeChild(myNode1.firstChild);
    }
    //clear word space
    let myNode2 = document.getElementById("wordsDiv");
    while (myNode2.firstChild) {
        myNode2.removeChild(myNode2.firstChild);
    }
    
    for (let i = 0; i < typedText.length; i++) {
        if(typedText[i] === " " || typedText[i] === "\n")
            continue;
        if(letterCounts[typedText[i]] === undefined) {
            letterCounts[typedText[i]] = 1;
        } else {
            letterCounts[typedText[i]]++;
        }
    }

    for (let i = 0; i < typedText.length; i++) {
        if(typedText[0] === "\n")
            i ++;
        let index = typedText.indexOf(" ");
        if(index == -1){
            if(wordCounts[typedText.substring(i, typedText.length)] === undefined) {
                wordCounts[typedText.substring(i, typedText.length)] = 1;
            } else {
                wordCounts[typedText.substring(i, typedText.length)] ++;
            }
            break;
        }

        if(wordCounts[typedText.substring(i, index)] === undefined) {
            wordCounts[typedText.substring(i, index)] = 1;
        } else {
            wordCounts[typedText.substring(i, index)] ++;
        }

        typedText = typedText.substring(index + 1, typedText.length);
        i = -1;
    }
    for(let letter in letterCounts){
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }
    document.getElementById("lettersDiv").appendChild(document.createElement("br"));
    document.getElementById("lettersDiv").appendChild(document.createElement("br"));
    for(let word in wordCounts){
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }
    letterCounts = {};
    wordCounts = {};
}